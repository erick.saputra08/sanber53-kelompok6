<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray dark:text-gray leading-tight">
            {{ __('Detail Question') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
                <div class="max-w-xl">
                    <h1>{{ $tanyabyid->judul }}</h1>
                    <p>{{ $tanyabyid->isi }}</p>
                    <img src="{{ asset('/img/' . $tanyabyid->lampiran) }}" alt="" srcset=""
                        class="rounded mx-auto d-block my-3">
                    <button disabled class="btn btn-secondary btn-sm">{{ $tanyabyid->status }}</button>
                    <br>

                    @if (Auth::check() && $tanyabyid->user_id == Auth::user()->id)
                        <a href="/tanya/{{ $tanyabyid->id }}/edit " class="btn btn-warning btn-sm my-2">Edit</a>
                    @endif

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
