<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
      <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      @auth
          <li class="nav-item">
            <form method="POST" action="{{ route('logout') }}">
              @csrf
              <a href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();" class="nav-link">
                  {{ __('Log Out') }}
              </a>
          </form>
        </li>
      @else
          <li class="nav-item">
              <a href="{{ route('login') }}" class="nav-link">Log in</a>
          </li>
          @if (Route::has('register'))
              <li class="nav-item">
                  <a href="{{ route('register') }}" class="nav-link">Register</a>
              </li>
          @endif
      @endauth

</nav>
