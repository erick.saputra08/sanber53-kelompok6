<button {{ $attributes->merge(['type' => 'submit', 'class' => 'inline-flex items-center px-4 py-2 bg-red border
 border-transparent rounded-md font-semibold text-xs text-black uppercase tracking-widest hover:bg-red active:
 bg-red focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-offset-2 dark:focus:ring-offset-gray
 transition ease-in-out duration-150']) }}>
    {{ $slot }}
</button>
