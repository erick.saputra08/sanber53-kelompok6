<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\KategoriController;
use App\Http\Controllers\JawabanController;
use App\Http\Controllers\PertanyaanController;

use App\Http\Controllers\QuestionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('welcome');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
        Route::get('/questions', [PertanyaanController::class, 'index'])->name('pertanyaan');
        Route::get('/questions/create', [PertanyaanController::class, 'create'])->name('pertanyaan.create');
        Route::post('/questions', [PertanyaanController::class, 'store'])->name('pertanyaan.store');
        Route::get('/questions/{pertanyaan}', [PertanyaanController::class, 'show'])->name('pertanyaan.show');
        Route::get('/questions/{pertanyaan}/edit', [PertanyaanController::class, 'edit'])->name('pertanyaan.edit');
        Route::put('/questions/{pertanyaan}', [PertanyaanController::class, 'update'])->name('pertanyaan.update');
        Route::delete('/questions/{pertanyaan}', [PertanyaanController::class, 'destroy'])->name('pertanyaan.destroy');
});

//kategori
Route::middleware('auth')->group(function () {
    Route::get('/kategori', [KategoriController::class, 'index'])->name('kategori');
    Route::get('/kategori/create', [KategoriController::class, 'create'])->name('kategori.create');
    Route::post('/kategori', [KategoriController::class, 'store'])->name('kategori.store');

    Route::get('/kategori/{kategori}', [KategoriController::class, 'show'])->name('kategori.show');
    
    Route::get('/kategori/{kategori}/edit', [KategoriController::class, 'edit'])->name('kategori.edit');

    Route::put('/kategori/{kategori}', [KategoriController::class, 'update'])->name('kategori.update');

    Route::delete('/kategori/{kategori}', [KategoriController::class, 'destroy'])->name('kategori.destroy');
});


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

// CRUD jawaban 
Route::get('jawaban', [JawabanController::class, "index"]);

Route::get('jawaban/create/{id}', [JawabanController::class, "create"]);

Route::post('jawaban', [JawabanController::class, "store"]);

Route::get('jawaban/{id}/edit', [JawabanController::class, "edit"]);

Route::put('jawaban/{id}', [JawabanController::class, "update"]);

Route::delete('jawaban/{id}', [JawabanController::class, "destroy"]);
